import Vue from "vue";
//import firebase from "firebase";
import App from "./App.vue";
import router from "./router";
import VueMaterial from "vue-material";
import "vue-material/dist/vue-material.min.css";
import Toasted from "vue-toasted";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCoffee } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(faCoffee);

Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.use(Toasted);
Vue.use(VueMaterial);
Vue.config.productionTip = false;

// Register a global custom directive called `v-focus`
Vue.directive("auto-focus", {
  // When the bound element is inserted into the DOM...
  inserted: function(el) {
    // Focus the element
    el.focus();
  }
});

const firebase = require("firebase/app");

let app = "";
/* Fire APP Config
 * For the simplicity of the app, I put the Firebase configuration in main.js file,
 * but on real production system, the config should be in a specific configuration file
 */
const configFireBase = {
  apiKey: "AIzaSyB24ScW967-b14Bl_uW4jKuWwisf_ENBB0",
  authDomain: "shoplist-gh.firebaseapp.com",
  databaseURL: "https://shoplist-gh.firebaseio.com",
  projectId: "shoplist-gh",
  storageBucket: "shoplist-gh.appspot.com",
  messagingSenderId: "114498070388"
};

firebase.initializeApp(configFireBase);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount("#app");
  }
});
