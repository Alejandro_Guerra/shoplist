# Shopping List


### Set up

install dependencies

Run ```npm install``` in webapp root

### Run in localhost for development

Run ```npm run dev``` in webapp root


### To create a production build, run npm run build.

Run ```npm run build```