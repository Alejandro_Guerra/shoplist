import axios from "axios";

export default {
  /*
   * GET all Users registered
   */
  getUsers(succes) {
    axios({
      method: "get",
      url: "https://shoplist-gh.firebaseio.com/users.json",
      timeout: 5000
    })
      .then(response => {
        succes(response.data);
      })
      .catch(err => {
        return err;
      });
  },
  /*
   * GET user from email registered
   */
  getUserById(id, succes) {
    axios({
      method: "get",
      url: "https://shoplist-gh.firebaseio.com/users.json",
      timeout: 5000
    })
      .then(response => {
        let user = "";
        for (let i = 0; i < Object.keys(response.data).length; i++) {
          if (id === response.data[Object.keys(response.data)[i]].email) {
            user = response.data[Object.keys(response.data)[i]];
          }
        }
        succes(user);
      })
      .catch(err => {
        return err;
      });
  },
  /*
   * Put new items to user list
   */
  addItemToUserList(id, params, succes) {
    axios({
      method: "put",
      url: "https://shoplist-gh.firebaseio.com/users/" + id + ".json",
      timeout: 5000,
      headers: {
        "Content-Type": "application/json"
      },
      data: params
    })
      .then(response => {
        succes(response.data);
      })
      .catch(err => {
        return err;
      });
  },
  /*
   * POST New user, after creat an account with email and pass, create
   * a new user with this info plus name and item list
   */
  postUser(params, succes) {
    axios({
      method: "post",
      url: "https://shoplist-gh.firebaseio.com/users.json",
      timeout: 5000,
      headers: {
        "Content-Type": "application/json"
      },
      data: params
    })
      .then(response => {
        succes(response.data);
      })
      .catch(err => {
        return err;
      });
  }
};
