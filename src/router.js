import firebase from "firebase";
import Vue from "vue";
import Router from "vue-router";

import Home from "@/views/Home";
import Login from "@/views/Login";
import SignUp from "@/views/SignUp";
import LandingPage from "@/views/LandingPage";
import About from "@/views/About";
import Doc from "@/views/Doc";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/landing",
      name: "LandingPage",
      component: LandingPage
    },
    {
      path: "/",
      component: LandingPage
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/sign-up",
      name: "SignUp",
      component: SignUp
    },
    {
      path: "/home",
      name: "Home",
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/about",
      name: "About",
      component: About
    },
    {
      path: "/doc",
      name: "Doc",
      component: Doc
    }
  ]
});

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) next("login");
  else if (!requiresAuth && currentUser) next("home");
  else next();
});

export default router;
